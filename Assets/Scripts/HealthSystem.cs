﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthSystem : MonoBehaviour {

    private int maxHeartAmount = 5;
    public int startHeart = 1;
    public int currentHealth;
    private int maxHealth;
    private int healthpPerHeart = 2;

    [SerializeField]
    GameObject deathUI;

    public Image[] healthImages;
    public Sprite[] healthSprites;

    Animator anim;

	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();

        deathUI.gameObject.SetActive(false);

        currentHealth = startHeart * healthpPerHeart;
        maxHealth = maxHeartAmount * healthpPerHeart;
        checkHealth();
	}

    void Update()
    {       
        if (currentHealth <= 0)
        {
            anim.SetBool("isDead", true);
            
            deathUI.gameObject.SetActive(true);

            GetComponent<PlayerControls>().enabled = false;
        }
    }

    void checkHealth()
    {
        for (int i = 0; i < maxHeartAmount; i++)
        {
            if (startHeart <= i)
            {
                healthImages[i].enabled = false;
            }
            else
            {
                healthImages[i].enabled = true;
            }
        }
        UpdateHeart();
    }

    void UpdateHeart()
    {
        bool empty = false;
        int i = 0;

        foreach (Image image in healthImages)
        {
            if (empty)
            {
                image.sprite = healthSprites[0];
            }
            else
            {
                i++;
                if (currentHealth >= i * healthpPerHeart)
                {
                    image.sprite = healthSprites[healthSprites.Length - 1];
                }
                else
                {
                    int currentHeartHealth = (int)(healthpPerHeart - (healthpPerHeart * i - currentHealth));
                    int healthPerImage = healthpPerHeart / (healthSprites.Length - 1);
                    int imageIndex = currentHeartHealth / healthPerImage;
                    image.sprite = healthSprites[imageIndex];
                    empty = true;
                }
            }
        }
    }

    public void TakeDamage(int amount)
    {
        currentHealth -= amount;
        currentHealth = Mathf.Clamp(currentHealth, 0, startHeart * healthpPerHeart);
        UpdateHeart();
    }

    public void addHeartContainer()
    {
        startHeart++;
        startHeart = Mathf.Clamp(startHeart, 0, maxHeartAmount);

        //add life
        //currentHealth = startHealth * healthpPerHeart;
        //maxHealth = maxHealthAmount * healthpPerHeart;

        checkHealth();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Acid")
        {
            TakeDamage(1);
        }

        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("ouch");
            TakeDamage(1);
        }

    }
}
