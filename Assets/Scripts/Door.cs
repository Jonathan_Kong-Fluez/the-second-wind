﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{

    Animator anim;

    Game_Manager gm;

    [SerializeField]
    GameObject doorType;

    int stateDoor = 1;
    string Door_name;

    public int Next_Level;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (getDoorState() == 3)
        {
            gm.LoadNextLevel(Next_Level);
        }
    }

    void Start()
    {
        gm = FindObjectOfType<Game_Manager>();

        anim = GetComponent<Animator>();

        if (doorType.name == "Entry_Door")
        {
            anim.SetFloat("DoorState", 3);
           // Open_Door();
        }

        if (doorType.name == "Next_Stage_Door")
        {
            Locked_Door();
        }

        if (doorType.name == "Boss_Door")
        {
            Locked_Door();
        }

    }

    void Locked_Door()
    {
        if (doorType.name == "Next_Stage_Door" || doorType.name == "Boss_Door" || doorType.name == "Exit_Door")
        {
            anim.SetFloat("DoorState", 1);
            stateDoor = 1;
        }

    }

    void Unlocked_Door()
    {
        if (doorType.name == "Next_Stage_Door" || doorType.name == "Boss_Door" || doorType.name == "Exit_Door")
        {
            anim.SetFloat("DoorState", 2);
            stateDoor = 2;
        }
    }

    public void Open_Door()
    {
        if (doorType.name == "Next_Stage_Door" || doorType.name == "Boss_Door" || doorType.name == "Exit_Door")
        {
            anim.SetFloat("DoorState", 3);
            stateDoor = 3;
        }
    }

    public void setDoorState( int state )
    {
        if (doorType.name == "Next_Stage_Door" || doorType.name == "Boss_Door" || doorType.name == "Exit_Door")
        {
            if (state == 1)
            {
                Locked_Door();
            }
            if (state == 2)
            {
                Unlocked_Door();
            }
            if (state == 3)
            {
                Open_Door();
            }
        }

    }

    public int getDoorState()
    {
        return stateDoor;
    }
}