﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour {

    bool sliding = false;

    float slideDuration = 0f;

    public float maxSlideDuration = 1.5f;

    [SerializeField]
    GameObject robotColider;

    public PlayerEnergy energy;

    public float topSpeed = 10f;

    bool facingRight = true;

    Animator anim;

    bool grounded = false;

    public Transform groundCheck;

    float groundRadius = 0.2f;

    public float jumpForce = 700f;

    public LayerMask whatIsGround;

    public float minX;

    public float maxX;

    bool doubleJump = false;

    public Transform muzzle;

    public GameObject bullet;

    float fallMultiplier = 3.5f;

    void Start()
    {
        energy = GetComponent<PlayerEnergy>();
        anim = GetComponent<Animator>();
        anim.SetBool("isDead", false);
    }

    void FixedUpdate()
    {   
        //true or false to check is ground hit with colision detection
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);

        anim.SetBool("Ground", grounded);

        //reset double jump
        if (grounded)
        {
            doubleJump = false;
        }

        anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);

        float move = Input.GetAxis("Horizontal") * Time.deltaTime * topSpeed;

        transform.Translate(new Vector3(move, 0, 0));

        Vector3 clamp = transform.position;

        clamp.x = Mathf.Clamp(transform.position.x ,minX, maxX);

        transform.position = clamp;
        //add velocity
        //GetComponent<Rigidbody2D>().velocity = new Vector2(move * topSpeed, GetComponent<Rigidbody2D>().velocity.y);

        anim.SetFloat("Speed", Mathf.Abs(move));

        if (move > 0 && !facingRight)
            Flip();
        else if (move < 0 && facingRight)
            Flip();
    }

    void Update()
    {
        if (energy.getEnergy())
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if ((grounded || !doubleJump))
                {
                    anim.SetBool("Ground", false);

                    energy.useEnergy();

                    GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));

                }

                if (!doubleJump && !grounded)
                {
                    doubleJump = true;
                }
            }

            if (GetComponent<Rigidbody2D>().velocity.y < 0)
            {
                GetComponent<Rigidbody2D>().velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
            }

            if (Input.GetButtonDown("Fire1"))
            {
                energy.useEnergy();
                GameObject mBullet = Instantiate(bullet, muzzle.position, muzzle.rotation);
                mBullet.GetComponent<Renderer>().sortingLayerName = "Player";

                anim.SetBool("isShooting", true);

            }

            if (Input.GetButtonUp("Fire1"))
            {  
                anim.SetBool("isShooting", false);
                anim.SetBool("isRunning_Shooting", false);
            }

            if (Input.GetButtonDown("Fire1") && GetComponent<Rigidbody2D>().velocity.x > 0)
            {
                anim.SetBool("isRunning_Shooting", true);
            }

            if (Input.GetButtonDown("Sliding") && !sliding)
            {
                slideDuration = 0f;

                anim.SetBool("isSliding", true);

                energy.useEnergy();

                gameObject.GetComponent<CapsuleCollider2D>().enabled = false;

                robotColider.GetComponent<CapsuleCollider2D>().enabled = false;

                sliding = true;
            }

            if (sliding)
            {
                slideDuration += Time.deltaTime;

                //topSpeed += 1.5f;

                if (slideDuration > maxSlideDuration)
                {
                    sliding = false;

                    anim.SetBool("isSliding", false);

                    gameObject.GetComponent<CapsuleCollider2D>().enabled = true;

                    robotColider.GetComponent<CapsuleCollider2D>().enabled = true;
                }
            }
        }

    }

    void Flip()
    {
        facingRight = !facingRight;

        Vector3 theScale = transform.localScale;

        theScale.x *= -1;

        transform.localScale = theScale;

    }
}
