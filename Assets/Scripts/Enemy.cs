﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public LayerMask enemyMask;

    public int health = 2;

    public float maxSpeed;
    public float minSpeed;
    Rigidbody2D myBody;
    Transform myTrans;
    float width, height;

    private PlayerControls thePlayer;

    public float playerRange;

    public GameObject bullet;

    public Transform muzzle;

    public LayerMask playerMask;

    public bool inRange = false;

    Animator anim;

    bool allowFire = true;

    bool flip = false;

    public float fireRate;

    bool isDead = false;

    // Use this for initialization
    void Start ()
    {
        anim = GetComponent<Animator>();
        myTrans = this.transform;
        myBody = this.GetComponent<Rigidbody2D>();
        SpriteRenderer mySprite = this.GetComponent<SpriteRenderer>();
        width = mySprite.bounds.extents.x;
        height = mySprite.bounds.extents.y;

        thePlayer = FindObjectOfType<PlayerControls>();

    }

    void FixedUpdate()
    {
        if (!isDead)
        {
            Vector3 currRot = myTrans.eulerAngles;

            // check to see if there's ground in front of us before moving
            Vector2 lineCastPos = myTrans.position.toVector2() - myTrans.right.toVector2() * width; // + Vector2.up * height;

            Vector2 lineCastVision = myTrans.position.toVector2() - myTrans.right.toVector2() * width;

            // check if the edge of the object is touching the ground
            Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down);
            bool isGrounded = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down, enemyMask);

            // check if you are about to hit another block
            Debug.DrawLine(lineCastPos, lineCastPos - myTrans.right.toVector2() * 0.1f);
            bool isBlocked = Physics2D.Linecast(lineCastPos, lineCastPos - myTrans.right.toVector2() * 0.1f, enemyMask);

            //bool playerinSights = Physics2D.OverlapCircle(transform.position, playerRange, playerMask);

            Debug.DrawLine(lineCastPos, lineCastPos - myTrans.right.toVector2() * playerRange);
            bool playerinSights = Physics2D.Linecast(lineCastPos, lineCastPos - myTrans.right.toVector2() * playerRange, playerMask);

            //prevent frontflipping on uneven ground
            myTrans.rotation = Quaternion.Euler(0, currRot.y, 0);

            if (playerinSights && allowFire)
            {
                Debug.Log("fire");
                anim.SetBool("isInRange", true);
                StartCoroutine(playerInRange());
            }

            // if there is no ground turn around or if i am blocked 
            if (!isGrounded || isBlocked)
            {
                flip = !flip;
                currRot.y += 180;
                myTrans.eulerAngles = currRot;
            }

            if (health <= 0)
            {
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
                isDead = true;
                anim.SetBool("isDead", true);
                Destroy(gameObject, 3f);
            }

            float speed = Mathf.Floor(Random.Range(minSpeed, maxSpeed));

            // alwayse move forward
            Vector2 myVel = myBody.velocity;

            //myVel.y = 0.2f;
            myVel.x = -myTrans.right.x * speed;

            //myVel.x -= speed;

            myBody.velocity = myVel;
        }
    }

    IEnumerator playerInRange()
    {
        allowFire = false;
        GameObject mBullet = Instantiate(bullet, muzzle.position, muzzle.rotation);
        mBullet.GetComponent<Renderer>().sortingLayerName = "Player";
        yield return new WaitForSeconds(fireRate);
        anim.SetBool("isInRange", false);
        allowFire = true;
    }

    public bool getFlip()
    {
        return flip;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerBullet")
        {
            health--;
        }
     }
}