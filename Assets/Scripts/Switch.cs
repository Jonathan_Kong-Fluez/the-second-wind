﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    [SerializeField]
    GameObject switch_ON;

    [SerializeField]
    GameObject switch_OFF;

    public bool isOn = false;

	// Use this for initialization
	void Start ()
    {
        // set the switch to off sprite
        gameObject.GetComponent<SpriteRenderer>().sprite = switch_OFF.GetComponent<SpriteRenderer>().sprite;
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = switch_ON.GetComponent<SpriteRenderer>().sprite;

        // set isOn to true when triggered
        isOn = true;
    }
}
