﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game_Manager : MonoBehaviour
{
    [SerializeField]
    GameObject switch_exit_level;

    [SerializeField]
    GameObject switch_Boss_Level;

    [SerializeField]
    GameObject Boss_Door;

    [SerializeField]
    GameObject exit_door;

    public void restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void quitGame()
    {
        Application.Quit(); 
    }

    public void LoadNextLevel(int x)
    {
        SceneManager.LoadScene(x);
    }

    public void getDoorState()
    {
        if (switch_exit_level != null)
        {
            if (switch_exit_level.GetComponent<Switch>().isOn == true)
            {
                exit_door.GetComponent<Door>().Open_Door();
            }
        }

        if (switch_Boss_Level != null)
        {
            if (switch_Boss_Level.GetComponent<Switch>().isOn == true)
            {
                Boss_Door.GetComponent<Door>().Open_Door();
            }
        }
        
    }

	// Update is called once per frame
	void Update ()
    {
        getDoorState();
    }
}
