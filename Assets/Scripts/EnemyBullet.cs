﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{

    public Enemy enemy;

    public float bulletSpeed;

    void Start()
    {
        enemy = FindObjectOfType<Enemy>();

        if (enemy.getFlip())
        {
            if (enemy.transform.localScale.x < 0)
            {
                bulletSpeed = -bulletSpeed;

                gameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
        }
        else
        {
            if (enemy.transform.localScale.x > 0)
            {
                bulletSpeed = -bulletSpeed;

                gameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
        }


    }

    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(bulletSpeed, GetComponent<Rigidbody2D>().velocity.y);
        Destroy(gameObject, 0.5f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
