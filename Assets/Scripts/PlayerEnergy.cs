﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerEnergy : MonoBehaviour
{

    [SerializeField]
    Slider energyBar;
    [SerializeField]
    Text text;

    float maxEnergy = 100;

    float currentEnergy;

    bool enoughEnergy = true;

    void Start()
    {
        energyBar.value = maxEnergy;
        currentEnergy = energyBar.value;
    }

    public void useEnergy()
    {
        currentEnergy = currentEnergy - 10f;
    }

    public bool getEnergy()
    {
        return enoughEnergy;
    }

    void Update()
    {
        energyBar.value = currentEnergy;

        if (currentEnergy < maxEnergy)
        {
            currentEnergy += 10 * Time.deltaTime;

        }

        if (currentEnergy > 10)
        {
            enoughEnergy = true;
        }
        else
        {
            enoughEnergy = false;
        }
        text.text = currentEnergy.ToString() + "%";
    }
}
