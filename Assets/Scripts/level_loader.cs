﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ColorToPrefab
{
    public Color32 color;
    public GameObject prefab;
    
}

public class level_loader : MonoBehaviour
{
    public string LevelFileName;
    public string FolderName;

    //public Texture2D Level_Map;

    public ColorToPrefab[] colorToPrefab;

	// Use this for initialization
	void Start ()
    {
        LoadMap();
	}
	
    void EmptyMap()
    {
        // find all the children and destroy them
        while(transform.childCount > 0)
        {
            Transform c = transform.GetChild(0);
            c.SetParent(null);  // having no parent
            Destroy(c.gameObject); // destroy the child 
            
        }
    }

    void LoadMap()
    {
        EmptyMap();

        // read streaming assets
        string filePath = Application.dataPath + "/StreamingAssets/" + FolderName + "/" + LevelFileName;
        byte[] bytes = System.IO.File.ReadAllBytes(filePath);
        Texture2D Level_Map = new Texture2D(2, 2);
        Level_Map.LoadImage(bytes);

        // get the raw pixels from the level image map

        Color32[] allPixels = Level_Map.GetPixels32();
        int width = Level_Map.width;
        int height = Level_Map.height;


        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                SpawnTileAt(allPixels[(y * width) + x], x, y);  // single array to go up the row second row = ( y * width) + x || (1 * 100) + 5 = 105
            }
        }
    }

    void SpawnTileAt( Color32 c, int x, int y)
    {

        // if transparent , do nothing
       // Color32 trans = new Color32(255, 255, 255, 255);

        if (c.a <= 0) 
        {
            return;
        }

        // find the right color in our map

        //
        foreach(ColorToPrefab ctp in colorToPrefab)
        {
            if ( c.Equals(ctp.color))
            {
                // spawn prefav at the right location

                GameObject go = Instantiate(ctp.prefab, new Vector3(x, y, 0), Quaternion.identity);

                return;
            }
        }

       // Debug.LogError("No COlor to prefab found for: " + c.ToString());

    }
}
