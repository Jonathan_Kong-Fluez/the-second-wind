﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]
public class Boss_AI : MonoBehaviour
{
    public Transform target;

    public float updateRate = 2f;

    Seeker seeker;
    Rigidbody2D rb;

    public int health = 3;

    public Path path;

    public float speed = 200;
    //a way to change between force and impulse ( enum )
    public ForceMode2D force_Mode;

    [HideInInspector]
    public bool pathisEnded = false;

    public float nextWayPointDistance = 1;

    bool dead = false;

    int currentWaypoint = 0;

    bool searching_Player = false;

    public float VizionRange;

    bool withinRange = false;

    public LayerMask playerLayer;

    void Start()
    {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        if (target == null)
        {
            //if no player
            if (!searching_Player)
            {
                //if (withinRange)
               // {
                    searching_Player = true;
                    StartCoroutine(SearchForPlayer());
               // }
                //return false;
            }
            return;
        }

        // start a path from current pos to target pos, when done call the onPathComplete function
        seeker.StartPath(transform.position, target.position, onPathComplete);


        StartCoroutine(rescanGraph(1));
        StartCoroutine(UpdatePath());
        
    }

    IEnumerator rescanGraph(float time)
    {
        yield return new WaitForSeconds(time);

        // Recalculate all graphs
        AstarPath.active.Scan();
    }

    IEnumerator SearchForPlayer()
    {
        GameObject search_result = GameObject.FindGameObjectWithTag("Player");

        if (search_result == null)
        {
            yield return new WaitForSeconds(1f);
            StartCoroutine(SearchForPlayer());
        }
        else
        {
            target = search_result.transform;
            searching_Player = false;
            StartCoroutine(UpdatePath());

            yield return false;
        }

    }


    IEnumerator UpdatePath()
    {
        withinRange = Physics2D.OverlapCircle(transform.position, VizionRange, playerLayer);

        if (target == null)
        {
            //if no player
            if (!searching_Player)
            {
               // if (withinRange)
               // {
                    searching_Player = true;
                    StartCoroutine(SearchForPlayer());
               // }

                //return false;
            }
            yield return false;
        }

        // start a path from current pos to target pos, when done call the onPathComplete function
        seeker.StartPath(transform.position, target.position, onPathComplete);

        yield return new WaitForSeconds(1f / updateRate);

        StartCoroutine(UpdatePath());

    }

    public void onPathComplete(Path p)
    {

        //check if there is an error
        if (!p.error)
        {
            //set current path to that found path
            path = p;
            // set waypoint to 0 to start at the start of the path
            currentWaypoint = 0;
        }
    }

    private void FixedUpdate()
    {
        if (health <= 0)
        {
            dead = true;
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
            gameObject.GetComponent<CircleCollider2D>().enabled = false;

        }

        if (target == null)
        {
            return;
        }

        if (path == null)
        {
            return;
        }

        // if we reach final waypoint 
        if (currentWaypoint >= path.vectorPath.Count)
        {
            if (pathisEnded)
            {
                return;
            }
            pathisEnded = true;
            return;
        }

        pathisEnded = false;

        //dir to nect waypoint , position of the waypoint we are goind for minus our current pos = direction 
        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;

        dir *= speed * Time.fixedDeltaTime;

        if (!dead)
        {
            if (withinRange)
            {
                // moving the AI
                rb.AddForce(dir, force_Mode);
            }
            
        }

        float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);

        if (dist < nextWayPointDistance)
        {
            currentWaypoint++;
            return;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerBullet")
        {
            health--;
            Destroy(gameObject, 3f);
        }
    }

}
