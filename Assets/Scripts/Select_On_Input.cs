﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Select_On_Input : MonoBehaviour
{
    public EventSystem event_System;
    public GameObject selected_Object;

    private bool button_Selected;

    private void Update()
    {
        if (Input.GetAxisRaw("Vertical") != 0 && button_Selected == false)
        {
            event_System.SetSelectedGameObject(selected_Object);
            button_Selected = true;
        }
    }

    private void OnDisable()
    {
        button_Selected = false;
    }

}
